package ups;

public class App {
    public static void main(String[] args) throws Exception {

        Client client = null;
        CommunicationHandler ch = new CommunicationHandler();
        client = new Client(ch);
        GUI gui = new GUI(client, ch);
    }
}
