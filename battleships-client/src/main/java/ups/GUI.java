package ups;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;  
public class GUI {
    public JFrame mainWindow;
    public JPanel loginScreen;
    public JPanel game;
    public JPanel gameInfoPanel;
    public JPanel gameControlPanel;
    public JPanel allyButtonsPanel;
    public JPanel enemyButtonsPanel;
    public JButton [] allyButtons;
    public JButton [] enemyButtons;
    public JButton findGameButton;
    public JButton disconnectButton;
    public JLabel gameInfoLabel;
    public JLabel connection_info_label;
    public final Client client;
    public final CommunicationHandler ch;
    public GUI(Client cli, CommunicationHandler communicationHandler){
        this.client = cli;
        this.ch = communicationHandler;
        mainWindow = new JFrame("Battleships");
        loginScreen = new JPanel();
        JLabel server_ip_label = new JLabel("IP address: ");
        JLabel server_port_label = new JLabel("Port: ");
        JLabel nickname_label = new JLabel("Nickname: ");
        final JTextField server_ip_text = new JTextField("104.248.131.64");
        final JTextField server_port_text = new JTextField("10000");
        final JTextField nickname_text = new JTextField("josifekja");
        JButton send_connection_request_button = new JButton("Connect!");
        JButton send_nickname_request_button = new JButton("Send nickname!");
        connection_info_label = new JLabel();
        server_ip_text.setPreferredSize(new Dimension(100,30));
        server_port_text.setPreferredSize(new Dimension(100,30));
        nickname_text.setPreferredSize(new Dimension(100,30));

        loginScreen.add(server_ip_label);
        loginScreen.add(server_ip_text);
        loginScreen.add(server_port_label);
        loginScreen.add(server_port_text);
        loginScreen.add(nickname_label);
        loginScreen.add(nickname_text);
        loginScreen.add(send_connection_request_button);
        loginScreen.add(send_nickname_request_button);
        loginScreen.add(connection_info_label);

        client.setGUI(this);
        this.createGamePanel();
        this.createControlButtons();
        send_connection_request_button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                
                String ipv4String = server_ip_text.getText();
                String portString = server_port_text.getText();
                int portInt = Integer.parseInt(portString);
                try {
                    client.createConnection(ipv4String, portInt);
                } catch (Exception eee) {
                    //TODO: handle exception
                }
            }
        });

        send_nickname_request_button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String nicknameString = nickname_text.getText();
                try {
                    if(ch.isGoodNickname(nicknameString)){

                        client.sendMessage("NICKNAME|"+nicknameString+"\n");
                    }
                    else{
                        connection_info_label.setText("Invalid nickname! Enter only letters or numbers!");
                    }
                    
                } catch (Exception ee) {
                    //TODO: handle exception
                }
            }
        });

        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setPreferredSize(new Dimension(500,500));

        mainWindow.getContentPane().add(loginScreen);
        mainWindow.pack();
        mainWindow.setVisible(true);
    }

    public void createGamePanel(){
        this.game = new JPanel();
        game.setLayout(new GridLayout(2,2));
        gameInfoPanel = new JPanel();
        gameControlPanel = new JPanel();
        allyButtonsPanel = new JPanel(new GridLayout(10,10));
        enemyButtonsPanel = new JPanel(new GridLayout(10,10));
        gameInfoLabel = new JLabel("Generic message!");
        gameInfoPanel.add(gameInfoLabel);
        game.add(gameInfoPanel);
        game.add(gameControlPanel);
        game.add(allyButtonsPanel);
        game.add(enemyButtonsPanel);
    }

    public void createControlButtons(){
        findGameButton = new JButton("Find game!");
        findGameButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                /*      FIND NEW GAME       */ 
                client.sendMessage("FIND_GAME\n");
                System.out.println("In queue for game!");
            }
        });
        gameControlPanel.add(findGameButton);
        disconnectButton = new JButton("Disconnect!");
        disconnectButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                /* Disconnect from server */ 
                try {
                    client.sendMessage("DISCONNECT\n");
                    Thread.sleep(100);
                    client.endConnection();
                    System.out.println("Disconnected from server!");
                } catch (Exception eee) {
                    //TODO: handle exception
                }
            }
        });
        gameControlPanel.add(disconnectButton);
    }

    public JButton[] initBoardButtons(String board, boolean isAlly){
        JButton [] buttons = new JButton[100];
        JButton button;
        for(int i = 0; i < 100; i++){
            if(isAlly){
                if(board.charAt(i) == '0')
                    buttons[i] = new JButton("");
                if(board.charAt(i) == '1')
                    buttons[i] = new JButton("X");
                if(board.charAt(i) == '2'){
                    buttons[i] = new JButton("");
                    buttons[i].setBackground(Color.GREEN);
                }
                if(board.charAt(i) == '3'){
                    buttons[i] = new JButton("X");
                    buttons[i].setBackground(Color.RED);
                }
            }
            else{
                if(board.charAt(i) == '0'){
                    button = new JButton("");
                    setButtonText(button, i);
                    buttons[i] = button;
                }
                if(board.charAt(i) == '1'){
                    button = new JButton("X");
                    setButtonText(button, i);
                    buttons[i] = button;
                }
                if(board.charAt(i) == '2'){
                    button = new JButton("");
                    button.setBackground(Color.GREEN);
                    setButtonText(button, i);
                    buttons[i] = button;
                }
                if(board.charAt(i) == '3'){
                    button = new JButton("X");
                    button.setBackground(Color.RED);
                    setButtonText(button, i);
                    buttons[i] = button;
                }
                
            }
            
        }
        return buttons;
    }

    public void addButtonsToBoard(JPanel panel, JButton [] buttons){
        for(int i = 0; i < 100; i++){
            panel.add(buttons[i]);
        }
    }

    public void setButtonText(JButton b, int i){
        final JButton bb = b;
        final int ii = i;
        bb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                bb.setText("");
                client.sendMessage("ATTACK|"+ii+"\n");
            }
         });
        b = bb;
    }
}
