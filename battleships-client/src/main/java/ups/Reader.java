package ups;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Reader implements Runnable {
    private BufferedReader reader = null;
    private CommunicationHandler ch = null;
    private GUI gui = null;
    private Client client = null;
    private Socket s;
    public Reader(Socket s, CommunicationHandler ch, GUI gui, Client client){
        this.ch = ch;
        this.client = client;
        this.gui = gui;
        this.s = s;
        try {
            reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    @Override
    public void run(){
        String message = null;
        String []detokenized = null;
        while(true){
            boolean is_valid = false;
            try {
                message = reader.readLine();
                if(message != null){
                    System.out.println("Message received!");
                    System.out.println(message);
                    gui.gameInfoLabel.setText(message);
                    detokenized = ch.splitMessage(message);
                    
                    /*                                             */
                    /*                                             */
                    /*                                             */
                    /*                                             */
                    /*              MESSAGE DECODER                */
                    /*                                             */
                    /*                                             */
                    /*                                             */
                    /*                                             */
                    /*                                             */
                    if(detokenized.length > 1){
                        if(detokenized[1].equalsIgnoreCase("ATTACK")){
                            if(!detokenized[2].equalsIgnoreCase("NOT_IN_GAME")){
                                if(detokenized[2].equalsIgnoreCase("YOU")){
                                    if(detokenized[3].equalsIgnoreCase("NOT_YOUR_TURN") || detokenized[3].equalsIgnoreCase("ALREADY_ATTACKED")){
                                        is_valid = true;
                                    }
                                    else{
                                        int position = Integer.parseInt(detokenized[4]);
                                        if(detokenized[3].equalsIgnoreCase("NO_HIT")){
                                        gui.enemyButtons[position].setBackground(Color.GREEN);
                                        }
                                        else{
                                        gui.enemyButtons[position].setBackground(Color.RED);
                                        }
                                    }
                                
    
                                }
                                else{
                                    int position = Integer.parseInt(detokenized[4]);
                                    if(detokenized[3].equalsIgnoreCase("NO_HIT")){
                                        gui.allyButtons[position].setBackground(Color.GREEN);
                                    }
                                    else{
                                        gui.allyButtons[position].setBackground(Color.RED);
                                    }
    
                                }
                            }
                            is_valid = true;
                        }
                        if(detokenized[1].equalsIgnoreCase("NICKNAME")){
                            if(detokenized[2].equalsIgnoreCase("OK")){
                                gui.allyButtonsPanel.removeAll();
                                gui.enemyButtonsPanel.removeAll();
                                gui.mainWindow.setContentPane(gui.game);
                                gui.mainWindow.revalidate();
                                is_valid = true;
                            }
                            if(detokenized[2].equalsIgnoreCase("NICKNAME_ALREADY_USED"))
                            {
                                gui.mainWindow.setContentPane(gui.loginScreen);
                                gui.connection_info_label.setText("Nickname already in use!");
                                gui.mainWindow.revalidate();
                                is_valid = true;
                            }
                        }
                        if(detokenized[1].equalsIgnoreCase("FIND_GAME")){
                            /* CREATE BOARD */
                            try {
                                System.out.println("NEW GAME FOUND" + detokenized[2]);
                                /* CREATE BUTTONS */
                                gui.allyButtonsPanel.removeAll();
                                gui.enemyButtonsPanel.removeAll();
                                gui.allyButtons = gui.initBoardButtons(detokenized[2], true);
                                gui.addButtonsToBoard(gui.allyButtonsPanel, gui.allyButtons);
                                gui.enemyButtons = gui.initBoardButtons("0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", false);
                                gui.addButtonsToBoard(gui.enemyButtonsPanel, gui.enemyButtons);
                                System.out.println("All buttons initiated");
                                gui.allyButtonsPanel.revalidate();
                                gui.enemyButtonsPanel.revalidate();
                            } catch (Exception e) {
                                //TODO: handle exception
                            }
                            is_valid = true;
                        }
    
    
                        if(detokenized[1].equalsIgnoreCase("GAME_STATE")){
                            System.out.println("Game state received");
                            if(detokenized[2].equalsIgnoreCase("RECONNECT")){
                                try {
                                    System.out.println("RECONNECTING TO EXISTING GAME");
                                    /* CREATE BUTTONS */
                                    gui.mainWindow.setContentPane(gui.game);
                                    gui.mainWindow.revalidate();
                                    gui.allyButtonsPanel.removeAll();
                                    gui.enemyButtonsPanel.removeAll();
                                    gui.allyButtons = gui.initBoardButtons(detokenized[3], true);
                                    gui.addButtonsToBoard(gui.allyButtonsPanel, gui.allyButtons);
                                    gui.enemyButtons = gui.initBoardButtons(detokenized[4], false);
                                    gui.addButtonsToBoard(gui.enemyButtonsPanel, gui.enemyButtons);
                                    System.out.println("All buttons initiated");
                                    gui.allyButtonsPanel.revalidate();
                                    gui.enemyButtonsPanel.revalidate();
                                } catch (Exception e) {
                                    //TODO: handle exception
                                }
                            }
                            if(detokenized[2].equalsIgnoreCase("GAME_ENDED")){
                                System.out.println("Game ended received");
                                if(detokenized[3].equalsIgnoreCase("OPPONENT_DISCONNECTED")){
                                    System.out.println("Opponent disconnected received");
                                    gui.allyButtonsPanel.removeAll();
                                    gui.enemyButtonsPanel.removeAll();
                                    gui.allyButtonsPanel.revalidate();
                                    gui.enemyButtonsPanel.revalidate();
                                }
                            }
                            is_valid = true;
                        }
                        if(detokenized[1].equalsIgnoreCase("DISCONNECT")){
                            client.endConnection();
                            gui.mainWindow.setContentPane(gui.loginScreen);
                            gui.mainWindow.revalidate();
                        }
                        is_valid = true;
                    }
                    
                }
            } catch (IOException e) {
                gui.connection_info_label.setText("Invalid server!");
            }
            if(!is_valid){
                try {
                    s.close();
                } catch (Exception e) {
                    //TODO: handle exception
                }
            }
            is_valid = false;
        }
    }

    public void stop() {
		try {
			reader.close();
		} catch (IOException e) {
		}
	}
}
