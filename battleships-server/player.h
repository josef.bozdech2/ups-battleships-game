#ifndef __PLAYER_H__
#define __PLAYER_H__
#define STATE_CONNECTED 1
#define STATE_IN_QUEUE 2
#define STATE_IN_GAME 3
#define STATE_DISCONNECTED 0

typedef struct{
    int socket;
    int inGame;
    char *nickname;
    int game_id;
    int player_id;
}Player;

Player *create_player(int socket, char *nickname);
void change_player_state(Player *player, int state);
int isInGame(Player *player);
int change_game_id(Player *player, int game_id);
int free_player(Player **player);

#endif