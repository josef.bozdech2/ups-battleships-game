#include<pthread.h> 


#include <stdio.h> // perror, printf
#include <stdlib.h> // exit, atoi
#include <unistd.h> // read, write, close
#include <arpa/inet.h> // sockaddr_in, AF_INET, SOCK_STREAM, INADDR_ANY, socket etc...
#include <string.h> // memset
#include <ctype.h>
#include "game.h"
#include "player.h"
void *connection_handler(void *);

/* Players array */
Player **players = NULL; 
/* Games array */
Game **games = NULL;
/* Number of players created */
int player_count = 0;
/* Number of games created */
int game_count = 0;
/* Player position in array waiting for game */
int game_queue = -1;
int main(int argc , char *argv[])
{
	players = (Player **)malloc(sizeof(Player) * 2000); //up to 2k players can join
	games = (Game **)malloc(sizeof(Game) * 2000);
	int server_socket , client_socket , c , *new_sock;
	struct sockaddr_in server , client;
	char *message;
	int is_ipv4 = -1;
	int correct_port = -1;
	is_ipv4 = inet_pton(AF_INET, argv[1], &(server.sin_addr));
	int i = 0;
	if((!is_ipv4 && (strcmp(argv[1], "localhost") != 0))){
		printf("Faulty ipv4 address\n");
		return 0;
	}

	for(i = 0; i < strlen(argv[2]); i++){
		if(!isdigit(argv[2][i]))
		{
			printf("Port is not a digit\n");
			return 0;
		}
		
	}

	
	

	server_socket = socket(AF_INET , SOCK_STREAM , 0);
	if (server_socket == -1)
	{
		printf("Could not create socket");
	}
	


	server.sin_family = AF_INET;
	if((strcmp(argv[1], "localhost") == 0))
		server.sin_addr.s_addr = inet_addr("127.0.0.1");
	else
		server.sin_addr.s_addr = inet_addr(argv[1]);

	server.sin_port = htons( atoi(argv[2]) );
	
	if( bind(server_socket,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		puts("bind failed");
		return 1;
	}
	puts("bind done");
	
	listen(server_socket , 5);
	
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);
	while( (client_socket = accept(server_socket, (struct sockaddr *)&client, (socklen_t*)&c)) )
	{
		puts("Connection accepted");
		
		pthread_t sniffer_thread;
		new_sock = malloc(1);
		*new_sock = client_socket;
		
		if( pthread_create( &sniffer_thread , NULL ,  connection_handler , (void*) new_sock) < 0)
		{
			perror("could not create thread");
			return 1;
		}
		
		//pthread_join( sniffer_thread , NULL);
		puts("Handler assigned");
	}
	
	if (client_socket<0)
	{
		perror("accept failed");
		return 1;
	}
	
	return 0;
}
/* Adding player to array of players after establishing connection! */
void *add_player_to_array(int socket, char *nickname){
	char *nick = nickname;
	// nick[strlen(nick)-1] = '\0';

	printf("Create player task: %s Socket id %d\n", nick, socket);
	printf("Player count before adding player: %d\n", player_count);
	players[player_count] = create_player(socket, nick);
	change_player_state(players[player_count], STATE_CONNECTED);
	printf("Player created: %s with socket %d and state %d\n", players[player_count]->nickname, players[player_count]->socket, players[player_count]->inGame);
	player_count++;
	printf("Player count after adding player: %d\n", player_count);
}


int get_player_state_by_socket_id(int socket){
	int i = 0;
	for(i = 0; i < player_count; i++){
		if(players[i]->socket == socket){
			return players[i]->inGame;
		}
	}
	return -1;
}

int get_player_array_position_by_socket_id(int socket){
	int i = 0;
	for(i = 0; i < player_count; i++){
		if(players[i]->socket == socket){
			return i;
		}
	}
	return -1;
}

char *get_player_nickname_in_array_by_socket_id(int socket){
	int i = 0;
	for(i = 0; i < player_count; i++){
		if(players[i]->socket == socket){
			return players[i]->nickname;
		}
	}
	return NULL;
}

int is_player_nickname_in_array(char *nickname){
	int i = 0;
	char *nick = nickname;
	nick[strlen(nick)-1] = '\0';
	for(i; i < player_count; i++){
		printf("Player %d: %s\n", i, players[i]->nickname);
		if(!strcmp(players[i]->nickname, nickname)){
			printf("Player already exists!\n");
			return 1;
		}
		
	}
	return 0;
}



char** split_message(char *message, int *message_len){
    char *buf = malloc(strlen(message)*sizeof(message));
    strcpy(buf, message);
    int i = 0;
    int j = 0;
    char *p = strtok (buf, "|");
    char **array = malloc(200 * sizeof(char*));

    while (p != NULL)
    {
        array[i] = malloc(strlen(p) * sizeof(char));
        array[i++] = p;
        p = strtok (NULL, "|");
    }
    *message_len = i;
    return array;
}



int add_game_to_array(int sock1, int sock2){
	/* Create boards */
	Board *temp1 = create_board();
	Board *temp2 = create_board();
	set_board(temp1);
	sleep(1);
	set_board(temp2);
	/* Create game */
	games[game_count] = init_game(temp1, temp2, sock1, sock2, game_count);
	game_count++;
	return (game_count-1);
}

int get_game_pos_by_player_id(int sock){
	int i = 0;
	for(i = 0; i < game_count; i++){
		if((games[i]->player1) == sock || (games[i]->player2) == sock){
			return i;
		}
	}
	return -1;
}

void *connection_handler(void *server_socket)
{
	int sock = *(int*)server_socket;
	int message_param_count = 0;
	int read_size;
	int attack_pos;
	char attack_result;
	char *message , client_message[2000], server_answer[2000], attack_pos_buffer[2000];
	char **split_msg = NULL;
	char *nick = NULL;
	int socket_state = 0;
	int queue_state = 0;
	int player_position_in_array = -1;
	int game_no = -1;
	int game_position_in_array = -1;
	int game_state = 0;
	int player1_pos = 0, player2_pos = 0;
	// int is_name_exists = -1;
	message = "NEW|MESSAGE\n";
	write(sock , message , strlen(message));
	while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
	{
		printf("Message received: ");
		printf("%s\n", client_message);
		split_msg = split_message(client_message, &message_param_count);
		/*                              */
		/*                              */
		/*                              */
		/*                              */
		/*            ATTACK			*/
		/*                              */
		/*                              */
		/*                              */
		/*                              */
		if(!strcmp(split_msg[0], "ATTACK")){
			/* Zjisti, jestli je hrac ve hre */
			socket_state = get_player_state_by_socket_id(sock);
			if(socket_state != STATE_IN_GAME || socket_state == -1){
				/*      Pokud ne, fuck off       */
				/*                1              */
				printf("Player not in game or player not found!\n");
				strcpy(server_answer, "402|ATTACK|NOT_IN_GAME\n");
				write(sock, server_answer, strlen(server_answer));
			}
			else
			{
				/* Pokud ano, jdeme zjistovat dal */
				game_no = get_game_pos_by_player_id(sock);
				/* Pokud je hráč na tahu, může hrát */
				if(games[game_no]->player_on_move == sock){
					/* Přidat logiku na tah */
					attack_pos = atoi(split_msg[1]);
					attack_result = player_turn(games[game_no], sock, attack_pos);
					printf("Attacked position: %d\nAttack result: %c", attack_pos, attack_result);

					if(attack_result != '4'){
						if(attack_result == '3'){
							/*                4               */
							printf("ATTACK acknowledged\n");
							strcpy(attack_pos_buffer, "201|ATTACK|YOU|SHIP_HIT|");
							snprintf(server_answer, 2000, "%s%d\n", attack_pos_buffer, attack_pos);
							printf("Message sent: %s", server_answer);

							if(sock == games[game_no]->player1){
								/* 				ATTACK|YOU                */
								write(games[game_no]->player1, server_answer, strlen(server_answer));


								/*              ATTACK|ENEMY              */
								strcpy(attack_pos_buffer, "201|ATTACK|ENEMY|SHIP_HIT|");
								snprintf(server_answer, 2000, "%s%d\n", attack_pos_buffer, attack_pos);
								write(games[game_no]->player2, server_answer, strlen(server_answer));

							}
							else
							{
								/* 				ATTACK|YOU                */
								write(games[game_no]->player2, server_answer, strlen(server_answer));


								/*              ATTACK|ENEMY              */
								strcpy(attack_pos_buffer, "201|ATTACK|ENEMY|SHIP_HIT|");
								snprintf(server_answer, 2000, "%s%d\n", attack_pos_buffer, attack_pos);
								write(games[game_no]->player1, server_answer, strlen(server_answer));
							}
							

							/*	 				GAME OVER                  */
							if(is_game_over(games[game_no])){
								printf("GAMEOVER");
								strcpy(server_answer, "200|GAME_STATE|GAME_OVER\n");


								write(games[game_no]->player1, server_answer, strlen(server_answer));
								write(games[game_no]->player2, server_answer, strlen(server_answer));

								/*               REMOVE PLAYERS FROM GAME			*/
								player1_pos = get_player_array_position_by_socket_id(games[game_no]->player1);
								player2_pos = get_player_array_position_by_socket_id(games[game_no]->player2);
								players[player1_pos]->inGame = STATE_CONNECTED;
								players[player2_pos]->inGame = STATE_CONNECTED;
							}
						}
						else
						{

							/*						NO_HIT 								*/
							printf("ATTACK acknowledged\n");
							strcpy(attack_pos_buffer, "201|ATTACK|YOU|NO_HIT|");
							snprintf(server_answer, 2000, "%s%d\n", attack_pos_buffer, attack_pos);
							printf("Message sent: %s", server_answer);
							if(sock == games[game_no]->player1){
								/* 				ATTACK|YOU                */
								write(games[game_no]->player1, server_answer, strlen(server_answer));


								/*              ATTACK|ENEMY              */
								strcpy(attack_pos_buffer, "201|ATTACK|ENEMY|NO_HIT|");
								snprintf(server_answer, 2000, "%s%d\n", attack_pos_buffer, attack_pos);
								write(games[game_no]->player2, server_answer, strlen(server_answer));

							}
							else
							{
								/* 				ATTACK|YOU                */
								write(games[game_no]->player2, server_answer, strlen(server_answer));


								/*              ATTACK|ENEMY              */
								strcpy(attack_pos_buffer, "201|ATTACK|ENEMY|NO_HIT|");
								snprintf(server_answer, 2000, "%s%d\n", attack_pos_buffer, attack_pos);
								write(games[game_no]->player1, server_answer, strlen(server_answer));
							}

						}
					}
					else
					{
						/*             ATTACK BAD         */
						/*                4               */
						printf("Player tried to attacked already attacked spot\n");
						strcpy(server_answer, "400|ATTACK|YOU|ALREADY_ATTACKED\n");
						write(sock, server_answer, strlen(server_answer));
					}
					
				}
				else
				{
					/*      Pokud ne, fuck off        */
					/*                2               */
					strcpy(server_answer, "404|ATTACK|YOU|NOT_YOUR_TURN\n");
					printf("Player tried to fuck the system!\n");
					write(sock, server_answer, strlen(server_answer));
				}
				

			}
		}
		/*                              */
		/*                              */
		/*                              */
		/*                              */
		/*            NICKNAME			*/
		/*                              */
		/*                              */
		/*                              */
		/*                              */
		if(!strcmp(split_msg[0], "nickname")){
			// is_name_exists = is_player_nickname_in_array(split_msg[1]);
			// if(is_name_exists){
				// write(sock, "400|NICKNAME|ALREADY_IN_USE\n", strlen("400|NICKNAME|ALREADY_IN_USE\n"));
			// }
			// else
			// {
				if(strstr(split_msg[1], "\\") || strstr(split_msg[1], "|"))
				{
					write(sock, "401|NICKNAME|INVALID_NICKNAME\n", strlen("401|NICKNAME|INVALID_NICKNAME\n"));
				}
				else
				{
					is_player_nickname_in_array(split_msg[1]);
					write(sock , "200|NICKNAME|OK\n" , strlen("200|NICKNAME|OK\n"));	
					add_player_to_array(sock, split_msg[1]);
					printf("Nickname acknowledged, player created!\n");
				}
				
				
			// }
			
		}
		/*                              */
		/*                              */
		/*                              */
		/*           FIND_GAME			*/
		/*                              */
		/*                              */
		/*                              */
		/*                              */
		if(strstr(split_msg[0], "find_game\n")){

			/* Zjisti stav hráče */
			socket_state = get_player_state_by_socket_id(sock);
			/* Pokud state == STATE_CONNECTED, jdi dál */
			if(socket_state == STATE_CONNECTED){
				/* Pokud je fronta prázdná, přidej socket do fronty */
				
				if(game_queue == -1){
					game_queue = sock;
					printf("Player added to queue: %d\n", game_queue);
					player_position_in_array = get_player_array_position_by_socket_id(sock);
					change_player_state(players[player_position_in_array], STATE_IN_QUEUE);
					write(sock, "201|ADDED_TO_QUEUE\n", strlen("201|ADDED_TO_QUEUE\n"));
					printf("Player %d state: %d\n", sock, players[player_position_in_array]->inGame);
				}
				else
				{
					/* Pokud ne vytvoř hru */
					/*
					
					

								NEW GAME
					
					
					
					*/
					printf("New game created!\n");
					/* Create new game */
					/* Change player state to STATE_IN_GAME */
					player_position_in_array = get_player_array_position_by_socket_id(sock);
					change_player_state(players[player_position_in_array], STATE_IN_GAME);
					player_position_in_array = get_player_array_position_by_socket_id(game_queue);
					change_player_state(players[player_position_in_array], STATE_IN_GAME);
					/* Create game */
					game_position_in_array = add_game_to_array(sock, game_queue);
					
					strcpy(server_answer, "200|FIND_GAME|");
					strcat(server_answer, games[game_position_in_array]->board2->board_array);
					strcat(server_answer, "\n");
					printf("%s", server_answer);
					write(sock, server_answer, strlen(server_answer));
					strcpy(server_answer, "200|FIND_GAME|");
					strcat(server_answer, games[game_position_in_array]->board1->board_array);
					strcat(server_answer, "\n");
					printf("%s", server_answer);
					write(game_queue, server_answer, strlen(server_answer));
					/* Remove player from queue */
					game_queue = -1;
				}
				
			}
			else
			{
				/* Pokud state != STATE_CONNECTED odešli chybovou hlášku */
				if(socket_state == STATE_IN_QUEUE){
					write(sock, "400|ALREADY_IN_QUEUE\n", strlen("400|ALREADY_IN_QUEUE\n"));
				}
				else
				{
					write(sock, "401|ALREADY_IN_GAME\n", strlen("401|ALREADY_IN_GAME\n"));
				}
				
			}
		}
		/*                              */
		/*                              */
		/*                              */
		/*           GAME_STATE			*/
		/*                              */
		/*                              */
		/*                              */
		/*                              */
		if(!strcmp(split_msg[0], "GAME_STATE")){

			write(sock , "GAME_STATE|OK\n" , strlen("GAME_STATE|OK\n"));
			printf("Game state!\n");
		}
		/*                              */
		/*                              */
		/*                              */
		/*           DISCONNECT			*/
		/*                              */
		/*                              */
		/*                              */
		/*                              */
		if(strstr(split_msg[0], "disconnect")){
			if(sock == game_queue){
				printf("Removing a disconnected client %d from queue!\n", game_queue);
				game_queue = -1;
			}
			write(sock , "200|DISCONNECT|OK\n" , strlen("200|DISCONNECT|OK\n"));
			printf("200|DISCONNECT|OK\n");
			close(sock);
		}

		/* Vyčistit paměť split_msg */
	}
	
	if(read_size == 0)
	{
		puts("Client disconnected");
		if(sock == game_queue){
			game_queue = -1;
		}
		/* Check if disconnected player was in game */
		if(players[get_player_array_position_by_socket_id(sock)]->inGame == STATE_IN_GAME){
			if(sock == games[get_game_pos_by_player_id(sock)]->player1){
				write(games[get_game_pos_by_player_id(sock)]->player2, "409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n", strlen("409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n"));
			}
			else
			{
				write(games[get_game_pos_by_player_id(sock)]->player1, "409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n", strlen("409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n"));
			}
			
		}
		players[get_player_array_position_by_socket_id(sock)]->inGame = STATE_CONNECTED;
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
		if(sock == game_queue){
			game_queue = -1;
			printf("Removing disconnected client from queue!\nGame queue: %d\n", game_queue);
		}
		players[get_player_array_position_by_socket_id(sock)]->inGame = STATE_CONNECTED;
		if(get_player_state_by_socket_id(sock) == STATE_IN_GAME){

			if(sock == games[get_game_pos_by_player_id(sock)]->player1){
					write(games[get_game_pos_by_player_id(sock)]->player2, "409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n", strlen("409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n"));
				}
				else
				{
					write(games[get_game_pos_by_player_id(sock)]->player1, "409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n", strlen("409|GAME_STATE|GAME_ENDED|OPPONENT_DISCONNECTED\n"));
				}
		}
	}
		
	free(server_socket);
	
	return 0;
}

