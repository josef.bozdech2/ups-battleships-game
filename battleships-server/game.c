#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "game.h"
#include "player.h"
#include "board.h"


Game *init_game(Board *b1, Board *b2, int p1, int p2, int game_id, char *nickname1, char *nickname2){
    Game *temp;
    if(!b1 || !b2 || !p1 || !p2){
        return NULL;
    }
    temp = (Game *)malloc(sizeof(Game));
    if(!temp){
        return NULL;
    }
    temp->board1 = b1;
    temp->board2 = b2;
    temp->player1 = p1;
    temp->player2 = p2;
    temp->player1_nickname = (char *)malloc(sizeof(char) * strlen(nickname1));
    temp->player2_nickname = (char *)malloc(sizeof(char) * strlen(nickname2));
    strcpy(temp->player1_nickname, nickname1);
    strcpy(temp->player2_nickname, nickname2);
    temp->game_id = game_id;
    temp->player_on_move = p1;
    temp->game_state = GAME_OK;
    return temp;
}

char player_turn(Game *game, int player, uint target){
    char attack_result;
    /* Zjistit sock hráče */
    if(game->player_on_move == player){
        /* Pokud je na tahu hráč 1*/
        if(game->player1 == player){
            attack_result = is_hit(game->board1, target);
            /* Switch player */
            if(attack_result != '4')
            game->player_on_move = game->player2;
            return attack_result;
        }
        else
        {
            /* Pokud je na tahu hráč 2 */
            attack_result = is_hit(game->board2, target);
            /* Switch player */
            if(attack_result != '4')
            game->player_on_move = game->player1;
            return attack_result;
        }
        
    }
    else
    {
        return '0';
    }
}

int is_game_over(Game *game){
    printf("Board 1 ships: %d\nBoard 2 ships: %d\n", game->board1->ship_alive, game->board2->ship_alive);
    if(game->board1->ship_alive == 0){
        return 1;
    }
    if(game->board2->ship_alive == 0){
        return 1;
    }
    return 0;
}

int free_game(Game **game){
    free_board(&(*game)->board1);
    free_board(&(*game)->board2);
    free(*game);
    *game = NULL;
    return 0;
}

void set_game_state(Game *game, int state){
    game->game_state = state;
}

char *get_current_board_state(Game *game, char *nickname){
    int i = 101;
    char *temp = (char *)malloc(sizeof(char) * 2000);
    if(!strcmp(game->player1_nickname, nickname)){
        /* Ally board == board2 */
        strcpy(temp, game->board2->board_array);
        /* Pipe */
        strcpy(temp+100, "|");
        /* Enemy board = board1 */
        strcpy(temp+101, game->board1->board_array);
    }
    else
    {
        /* Ally board == board1 */
        strcpy(temp, game->board1->board_array);
        /* Pipe */
        strcpy(temp+100, "|");
        /* Enemy board = board2 */
        strcpy(temp+101, game->board2->board_array);
    }
    temp[201] = '\0';
    for(i; i < 202; i++){
        if(temp[i] == '1'){ 
            temp[i] = '0';
        }
    }
    printf("%s\n", temp);
    return temp;

}
