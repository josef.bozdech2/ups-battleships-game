#include <stdlib.h>
#include <stdio.h>
#include "info.h"


Info *create_info(Player **players, Game** games){
    Info *temp = (Info *)malloc(sizeof(Info));
    temp->games = games;
    temp->players = players;
    temp->game_count = 0;
    temp->player_count = 0;
    return temp;
}

void *add_player(Info** info, Player **player){
    (*info)->players[(*info)->player_count] = (*player);
    (*info)->player_count++;
}