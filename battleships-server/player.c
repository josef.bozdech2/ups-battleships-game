#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "player.h"

Player *create_player(int socket, char *nickname){
    Player *temp;
    if(!socket)
        return NULL;
    temp = (Player *)malloc(sizeof(Player));
    temp->socket = socket;
    temp->nickname = (char *)malloc(sizeof(char *) * strlen(nickname));
    strcpy(temp->nickname, nickname);
    return temp;
}
void change_player_state(Player *player, int state){
    player->player_state = state;
}

int isplayer_state(Player *player){
    return player->player_state;
}

int free_player(Player **player){
    free(*player);
}

int change_game_id(Player *player, int game_id){
    player->game_id = game_id;
}

void set_player_id(Player *player, int player_id){
    player->player_id = player_id;
}
