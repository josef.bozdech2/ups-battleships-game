#ifndef __GAME_H__
#define __GAME_H__
#define GAME_OK 0
#define GAME_WAIT 1
#include "board.h" 
#include "player.h"
typedef struct
{
    Board *board1;
    Board *board2;
    int player1;
    int player2;
    char *player1_nickname;
    char *player2_nickname;
    int game_id;
    int player_on_move;
    int game_state;
}Game;

Game *init_game(Board *b1, Board *b2, int p1, int p2, int game_id, char *nickname1, char *nickname2);
char player_turn(Game *game, int player, uint target);
int free_game(Game **game);
int is_game_over(Game *game);
void set_game_state(Game *game, int state);
char *get_current_board_state(Game *game, char *nickname);







#endif