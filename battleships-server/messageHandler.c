#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char** split_message(char *message, int *message_len){
    char *buf = malloc(strlen(message)*sizeof(char *));
    strcpy(buf, message);
    int i = 0;
    int j = 0;
    char *p = strtok (buf, "|");
    char **array = malloc(200 * sizeof(char*));

    while (p != NULL)
    {
        array[i] = malloc(strlen(p) * sizeof(char));
        array[i++] = p;
        p = strtok (NULL, "|");
    }
    for (j = 0; j < i; ++j) 
        printf("%s\n", array[j]);

    *message_len = i;
    return array;
}


int main(){
    int i = 0;
    int message_len = 0;
    char message[200] = "attack|1234|4567|7894\n";
    char *message2 = "test|123";
    char **msg = NULL;
    msg = split_message(message, &message_len);
    printf("message param count: %d\n", message_len);   
    
    if(!strcmp(msg[0], "attack")){
        printf("Attack acknowledged!\n");
	}
	else{
		printf("Default operation!\n");
	}

    msg = split_message(message2, &message_len);
    for (i = 0; i < message_len; ++i) 
        printf("%s\n", msg[i]);
    printf("message param count: %d\n", message_len);  
    if(!strcmp(msg[0], "attack")){

			printf("Attack acknowledged!\n");
		}
		else{
			printf("Default operation!\n");
		}
    
    return 0;
}



