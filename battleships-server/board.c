#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "board.h"
/**
 * Initialize board with its parameters
 * BOARD_SIZE
*/
Board *create_board(){
    Board *temp;
    int i = 0;
    temp = (Board *)malloc(sizeof(Board));
    if(!temp)
        return NULL;
    temp->x_size = BOARD_SIZE;
    temp->y_size = BOARD_SIZE;
    temp->board_array = (char *)malloc(sizeof(int) * temp->x_size * temp->y_size);
    for(i = 0; i < (temp->x_size * temp->y_size); i++){
        temp->board_array[i] = '0';
    }
    temp->ship_alive = SHIP_COUNT;
    return temp;
}

char is_hit(Board *board, uint pos){
    printf("Board attack position info: %s\n", board->board_array);
    if(board->board_array[pos]== '1' || board->board_array[pos]== '0'){
        if(board->board_array[pos] == '1'){
            board->board_array[pos]='3';
            board->ship_alive--;
            printf("Ship count: %d\n", board->ship_alive);
            return '3';
        }
        else
        {
            board->board_array[pos]='2';
            return '2';
        }
    }
    else
    {
        return '4';
    }
    
    
}

int free_board(Board **board){
    free((*board)->board_array);
    free(*board);
    *board = NULL;
    return 1;
}



int set_board(Board *board){
    int board_size = board->x_size*board->y_size;
    int ships_generated = 0;
    int r;
    int i = 0;
    srand(time(NULL));
    do
    {
        r = rand() % board_size;
        if(board->board_array[r] != '1'){
            board->board_array[r] = '1';
            ships_generated++;
        }
        
        
    } while (SHIP_COUNT!=ships_generated);
}

void print_board(Board *board){
    int i = 0, j = 0;
    
    printf("X");
    for(i = 0; i < (board->x_size); i++){
        printf("%c", i+65);
    }
    printf("\n");
    
    for(i = 0; i < (board->y_size); i++){
        printf("%i", i+1);
        for(j = 0; j < (board->x_size); j++){
            printf("%d", board->board_array[i*(board->x_size)+j]);
        }
        printf("\n");
    }
}