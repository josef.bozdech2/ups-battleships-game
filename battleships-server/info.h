#ifndef __INFO_H__
#define __INFO_H__
#include "player.h"
#include "game.h"
typedef struct{
    Player **players;
    Game **games;
    int player_count;
    int game_count;
}Info;


Info *create_info(Player **players, Game** games);
void *add_player(Info** info, Player **player);


#endif