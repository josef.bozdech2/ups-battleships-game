#ifndef __BOARD_H__
#define __BOARD_H__
#define BOARD_SIZE 10
#define SHIP_COUNT 12
typedef unsigned int uint;
typedef struct {
    uint x_size;
    uint y_size;
    char *board_array;
    uint ship_alive;
} Board;

Board *create_board();
int set_board(Board *board);
char is_hit(Board *board, uint pos);
int shoot_board(Board *board, uint x_pos, uint y_pos);
void print_board(Board *board);
int free_board(Board **board);

#endif